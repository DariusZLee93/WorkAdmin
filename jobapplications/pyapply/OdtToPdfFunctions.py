"""Convert ODT to PDF"""
import subprocess
import os

import PyPDF2

import PyApplyConstants as Consts

LIBRE_OFFICE_PATH = r'C:\Program Files\LibreOffice 5\program\soffice.exe'

# Test Paths
TEST_DIRECTORY = 'test-OdtToPdfFunctions'
TEST_APPLICATION_PDF_PATH = os.path.join(Consts.PROJECT_DIR_PATH,
                                         TEST_DIRECTORY,
                                         'Application.pdf')
TEST_COVER_LETTER_RENDERED_PDF_PATH = os.path.join(Consts.PROJECT_DIR_PATH,
                                                   TEST_DIRECTORY,
                                                   'CoverLetterRendered.pdf')
TEST_COVER_LETTER_RENDERED_ODT_PATH = os.path.join(Consts.PROJECT_DIR_PATH,
                                                   TEST_DIRECTORY,
                                                   'CoverLetterRendered.odt')
TEST_RESUME_PDF_PATH = os.path.join(Consts.PROJECT_DIR_PATH,
                                    TEST_DIRECTORY,
                                    'Resume.pdf')

def convert_to_pdf(input_odt_path, output_pdf_dir):
    """The primary function  to convert an ODT (input_odt_path) to an output path."""
    print('ODT to convert to pdf: ' + input_odt_path)
    if not os.path.isdir(output_pdf_dir):
        raise IOError('output_pdf_dir input parameter must point to a directory.')
    input_odt_path_full = get_full_path(input_odt_path)
    output_pdf_dir_full = get_full_path(output_pdf_dir)

    if check_if_libre_office_exists():
        subprocess.call([LIBRE_OFFICE_PATH, '--headless', '--convert-to',
                         'pdf', input_odt_path_full, '--outdir', output_pdf_dir_full])
    else:
        raise IOError('The libre office executable was not found.')

def get_full_path(path: str):
    """Get the absolute path of a relative path."""
    return os.path.abspath(path)

def check_if_libre_office_exists():
    """A function to check if the libre office executable is in place."""
    if os.path.exists(LIBRE_OFFICE_PATH):
        return True
    else:
        return False

def merge_pdfs(pdf_path_1, pdf_path_2, output_pdf_path):
    """Merge tow pdfs from two paths and output."""
    print('Merge pdfs from path: ' + pdf_path_1 + ' and path: ' + pdf_path_2 +
          ' to the path: ' + output_pdf_path)
    with open(pdf_path_1, mode='rb') as cl_file:
        with open(pdf_path_2, mode='rb') as resume_file:
            with open(output_pdf_path, mode='wb') as output_file:
                merger = PyPDF2.PdfFileMerger()
                merger.append(fileobj=cl_file, pages=(0, 1))
                merger.append(fileobj=resume_file, pages=(0, 1))
                merger.write(output_file)

def clean_up_tests():
    """Clean up test outputs."""
    os.remove(TEST_APPLICATION_PDF_PATH)
    os.remove(TEST_COVER_LETTER_RENDERED_PDF_PATH)

if __name__ == '__main__':
    convert_to_pdf(TEST_COVER_LETTER_RENDERED_ODT_PATH, TEST_DIRECTORY)
    merge_pdfs(TEST_COVER_LETTER_RENDERED_PDF_PATH,
               TEST_RESUME_PDF_PATH, TEST_APPLICATION_PDF_PATH)
