"""These functions extract and modify the content.xml in and ODT file."""
import re
import json
import zipfile
import os

import PyApplyConstants as Consts

TEST_FOLDER = os.path.join(Consts.PROJECT_DIR_PATH, 'test-OdtFunctions')
TEST_JSON_DATA_PATH = os.path.join(TEST_FOLDER, 'job_data.json')
TEST_COVER_LETTER_PATH = os.path.join(TEST_FOLDER, 'CoverLetter.odt')
TEST_COVER_LETTER_RENDERED_PATH = os.path.join(TEST_FOLDER, 'CoverLetterRendered.odt')

def remove_spans(data: str):
    """A function to clean up the content.xml file."""
    pattern = re.compile('<.?text:span.*?>')
    data = pattern.sub('', data)
    return data

def replace_keywords(content: str, data_to_inject: dict):
    """Replace all words in the content string that
    match keys in the data_to_inject dictionary."""
    for key in data_to_inject:
        if type(data_to_inject[key]) is str:
            moustache_key = '{{' + key + '}}'
            pattern = re.compile(moustache_key)
            content = pattern.sub(data_to_inject[key], content)
        else:
            print('Not a known type.')
    return content



def copy_odt_modify_content_xml(odt_original_path: str, new_odt_path: str, json_data: dict):
    """The main function of the OdtFunctions."""
    if os.path.exists(new_odt_path):
        os.remove(new_odt_path)
    odt_in = zipfile.ZipFile(odt_original_path, mode='r')
    odt_out = zipfile.ZipFile(new_odt_path, mode='w')
    for item in odt_in.infolist():
        buffer = odt_in.read(item.filename)
        if item.filename == 'content.xml':
            buffer = buffer.decode('utf-8')
            buffer = remove_spans(buffer)
            buffer = replace_keywords(buffer, json_data)
        odt_out.writestr(item, buffer)

def test():
    """The test function to be called if this utility file is called directly."""
    with open(TEST_JSON_DATA_PATH, mode='r') as json_file:
        json_data_as_string = str(json_file.read())
        json_data = json.loads(json_data_as_string)
        copy_odt_modify_content_xml(TEST_COVER_LETTER_PATH,
                                    TEST_COVER_LETTER_RENDERED_PATH,
                                    json_data)

if __name__ == '__main__':
    test()
