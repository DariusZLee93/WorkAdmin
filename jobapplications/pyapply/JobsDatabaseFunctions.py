"""Functions that manage the database."""
import os
import json
import sqlite3
import PyApplyConstants as Consts

TEST_FOLDER_PATH = os.path.join(Consts.PROJECT_DIR_PATH, 'test_JobsDatabaseFunctions')
TEST_DATA_PATH = os.path.join(TEST_FOLDER_PATH, 'job_data.json')

def create_cursor():
    """Create the cursor and open the connection."""
    conn = sqlite3.connect('job.db')
    cursor = conn.cursor()

    return cursor

def commit_and_close_connection(cursor: sqlite3.Cursor):
    """Commit and close the connection of a cursor."""
    conn = cursor.connection

    conn.commit()
    conn.close()

def execute_sql(sql_to_execute: str):
    """Helper function to create connections and execute sql
    then commit and close connection."""
    cur = create_cursor()
    result = cur.execute(sql_to_execute)
    commit_and_close_connection(cur)

    return result

def execute_sql_with_data(sql_to_execute: str, data: tuple):
    """Helper function to create connections and execute sql
    then commit and close connection."""
    print('Sql string: ' + sql_to_execute)
    print('Tuple data: ' + str(data))

    cur = create_cursor()
    result = cur.execute(sql_to_execute, data)
    commit_and_close_connection(cur)

    return result

def create_jobs_table():
    """Set up the jobs table."""
    create_table_sql = str('CREATE TABLE jobs(id INTEGER PRIMARY KEY ASC AUTOINCREMENT,' +
                           'companyName TEXT NOT NULL, companyAddress TEXT NOT NULL,' +
                           'companyCity TEXT NOT NULL, companyProvince TEXT NOT NULL,' +
                           'companyPostalCode TEXT NOT NULL, postingUrl TEXT NOT NULL UNIQUE,' +
                           'jobSkills TEXT NOT NULL, careerSiteUrl TEXT,' +
                           'careerSiteUserName TEXT, careerSitePassword TEXT);')
    result = execute_sql(create_table_sql)
    print('Result of create table sql: ' + result)

def insert_into_jobs(job_data: dict):
    """A function to insert into the jobs table."""
    insert_tuple = (job_data['companyName'], job_data['companyAddress'], job_data['companyCity'],
                    job_data['companyProvince'], job_data['companyPostalCode'],
                    job_data['postingUrl'], job_data['jobSkills'],
                    job_data.get('careerSiteUrl'), job_data.get('careerSiteUserName'),
                    job_data.get('careerSitePassword'))
    insert_into_sql = str('INSERT INTO jobs values(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)')
    execute_sql_with_data(insert_into_sql, insert_tuple)

def remove_from_jobs(url_to_delete: str):
    """The delete from db function on postingUrl."""
    delete_tuple = (url_to_delete,)
    delete_from_sql_string = str('DELETE FROM jobs WHERE postingUrl=?')
    execute_sql_with_data(delete_from_sql_string, delete_tuple)

if __name__ == '__main__':
    with open(TEST_DATA_PATH, 'r') as json_data_file:
        JOB_DATA_AS_DICT = json.load(json_data_file)
        POSTING_URL = JOB_DATA_AS_DICT['postingUrl']
        try:
            print('First insert')
            insert_into_jobs(JOB_DATA_AS_DICT)
        except sqlite3.IntegrityError:
            print('Value already exists. Test wasn\'t cleaned up properly.')
        try:
            print('Second insertion')
            insert_into_jobs(JOB_DATA_AS_DICT)
        except sqlite3.IntegrityError:
            print('Value already exists.')
        print('Delete record.')
        remove_from_jobs(POSTING_URL)
        print('Insert again.')
        insert_into_jobs(JOB_DATA_AS_DICT)
        print('Clean up and remove.')
        remove_from_jobs(POSTING_URL)
