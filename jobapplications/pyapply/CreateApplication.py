"""Call the sequences of operations to create an application."""
import CreateApplicationFunctions
import RenderFileFunctions
import RenderApplicationSetup
import JobsDatabaseFunctions

def main():
    """The main function. The order of operations to build an application"""
    print('Getting user application data.')
    user_job_data = CreateApplicationFunctions.getjobdatafromuser()
    JobsDatabaseFunctions.insert_into_jobs(user_job_data)

    user_application_choice = CreateApplicationFunctions.get_user_application_choice()

    RenderApplicationSetup.setup_environment(user_application_choice)

    print(user_job_data)
    RenderFileFunctions.Render(user_job_data)

    RenderApplicationSetup.copyrenderedapplication(user_application_choice,
                                                   user_job_data['companyName'])

if __name__ == '__main__':
    main()
