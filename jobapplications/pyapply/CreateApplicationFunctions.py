"""The functions required to create a job application."""
import os
import json

import PyApplyConstants as Consts

TEST_FOLDER = os.path.join(Consts.PROJECT_DIR_PATH, 'test_CreateApplicationFunctions')
TEST_JOB_DATA_JSON_PATH = os.path.join(TEST_FOLDER, 'job_data.json')

def getjobdatafromuser():
    """Get user input for the application to generate."""
    job_data = dict()
    job_data['companyName'] = input('Enter Company Name: ')
    job_data['companyAddress'] = input('Enter Company Address: ')
    job_data['companyCity'] = input('Enter Company City: ')
    job_data['companyProvince'] = input('Enter Company Province: ')
    job_data['companyPostalCode'] = input('Enter Company Postal Code: ')
    job_data['postingUrl'] = input('Enter Posting Url: ')

    job_data['jobSkills'] = enter_skills()

    career_site_exists = input('Enter Career Site Url (Press enter to skip): ')
    if not career_site_exists is "":
        job_data['career_site_exists'] = True
        job_data['careerSiteUrl'] = career_site_exists
        job_data['careerSiteUserName'] = input('Enter career site username: ')
        job_data['careerSitePassword'] = input('Enter career site password: ')

    return job_data

def array_to_keyword(input_array: list):
    """Create a single formatted string from an array of strings."""
    if len(input_array) == 1:
        return input_array[0]
    elif len(input_array) == 2:
        return input_array[0] + ' and ' + input_array[1]
    else:
        return input_array[0] + ', ' + array_to_keyword(input_array[1:])


def enter_skills():
    """A small input function to get a list of skills to insert into the application."""
    skills_to_enter = []
    while True:
        line = input('Enter skill (press enter to exit): ')
        if not line is "":
            skills_to_enter.append(line)
        else:
            break
    skills_as_string = array_to_keyword(skills_to_enter)
    return skills_as_string

def get_user_application_choice():
    """Gets the user application choice from folders found in .\applications"""
    applications = os.listdir(Consts.APPLICATIONS_PATH)
    while True:
        print('Applications available are: ' + str(applications).strip('[]'))
        app_choice = input('Choose an application : ')
        if app_choice in applications:
            return app_choice
        else:
            print('Not a real option, try again.')

if __name__ == '__main__':
    #JOB_DATA = getjobdatafromuser()
    #print(JOB_DATA)
    #with open(TEST_JOB_DATA_JSON_PATH, 'w') as json_file:
    #    json.dump(JOB_DATA, json_file)
    get_user_application_choice()
