"""This module is used for setting up the temp folder before processing is done by the render."""
import os
import shutil
import PyApplyConstants as Const

# Testing constants (For the main function.)
__testapplicationchoice__ = 'testapplication'
__testcompany__ = 'testcompany'

# Utility methods
def createtempfolder():
    """Create the temp folder. Delete contents if already exists."""
    if os.path.exists(Const.TEMP_FOLDER_PATH):
        shutil.rmtree(Const.TEMP_FOLDER_PATH)
        createtempfolder()
    else:
        os.makedirs(Const.TEMP_FOLDER_PATH)


def copyapplicationfilestotemp(application_choice: str):
    """Copy the files from the application folder into the temp folder."""
    application_choice_folder_path = Const.getapplicationfolderpath(application_choice)
    resume_file_path_with_choice = os.path.join(application_choice_folder_path, Const.RESUME_FILE)
    cover_letter_path_with_choice = os.path.join(application_choice_folder_path,
                                                 Const.COVER_LETTER_ODT_FILE)

    if os.path.exists(resume_file_path_with_choice) and \
          os.path.exists(cover_letter_path_with_choice):
        shutil.copyfile(resume_file_path_with_choice, Const.RESUME_TEMP_PATH)
        shutil.copyfile(cover_letter_path_with_choice, Const.COVER_LETTER_ODT_TEMP_PATH)
    else:
        error_message = str('Resume and/or Cover letter not found at path ' +
                            resume_file_path_with_choice + ' and/or ' +
                            cover_letter_path_with_choice)
        raise FileNotFoundError(error_message)

def createoutputfolder(application_choice: str, company_name: str):
    """Get the folder name. Logic is we append the number depending on how many
    # applications to the company there are. Also return the path to this folder."""
    application_choice_folder_path = Const.getapplicationfolderpath(application_choice)
    number_of_applications = 0
    while True:
        potential_folder_name = str(company_name + '_' + str(number_of_applications))
        potential_folder_path = os.path.join(application_choice_folder_path, potential_folder_name)
        if not os.path.exists(potential_folder_path):
            os.makedirs(potential_folder_path)
            return potential_folder_path
        else:
            number_of_applications += 1

def copyrenderedapplication(application_choice: str, company_name: str):
    """Copy the rendered file from the temp directory back to the application choice."""
    render_output_folder = createoutputfolder(application_choice, company_name)
    rendered_resume_file_path = Const.build_resume_path(company_name, render_output_folder)
    rendered_coverletter_file_path = Const.build_coverletter_path(company_name, render_output_folder)
    rendered_merged_file_path = Const.build_merged_path(company_name, render_output_folder)

    shutil.copy(Const.COVER_LETTER_PDF_TEMP_PATH, rendered_coverletter_file_path)
    print('Rendered resume file path: ' + rendered_resume_file_path)
    shutil.copy(Const.RESUME_TEMP_PATH, rendered_resume_file_path)
    shutil.copy(Const.APPLICATION_TEMP_PATH, rendered_merged_file_path)

def create_test_render():
    """Create test files to emulate a render."""
    open(Const.COVER_LETTER_PDF_TEMP_PATH, 'w').close()
    open(Const.RESUME_TEMP_PATH, 'w').close()
    open(Const.APPLICATION_TEMP_PATH, 'w').close()

def setup_environment(application_choice: str):
    createtempfolder()
    copyapplicationfilestotemp(application_choice)

# Main is used to test the functionality of the utilities.
if __name__ == '__main__':
    print(Const.TEMP_FOLDER_PATH)
    createtempfolder()
    print('Copyting application files to temp with choice ' + __testapplicationchoice__)
    copyapplicationfilestotemp(__testapplicationchoice__)
    print('Mimicking render operation.')
    create_test_render()
    print('Copying rendered applications to the choice with company name: ' + __testcompany__)
    copyrenderedapplication(__testapplicationchoice__, __testcompany__)
