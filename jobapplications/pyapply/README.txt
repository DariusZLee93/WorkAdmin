Rules for this folder:

1. Folder represents a {year|month}. 
2. Folder must have a resume, cover letter, and cover letter in handlebars format.
	a. Resume must be named "Resume.pdf"
	b. Cover letter be named "CoverLetter.odt"
	c. Cover letter (Handlebars) must be named "CoverLetter.handles"
