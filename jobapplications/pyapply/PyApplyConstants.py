"""Constants for the PyApply Project."""
import os

PROJECT_DIR_PATH = os.path.dirname(os.path.realpath(__file__))
PROJECT_USER_NAME = 'DariusLee'

# Cover letters, resume, and completed application paths.
__application_folder__ = 'applications'
APPLICATIONS_PATH = os.path.join(PROJECT_DIR_PATH, __application_folder__)

# Application files
COVER_LETTER_ODT_FILE = 'CoverLetter.odt'
COVER_LETTER_RENDERED_ODT_FILE = 'CoverLetterRendered.odt'
COVER_LETTER_PDF_FILE = 'CoverLetterRendered.pdf'
RESUME_FILE = 'Resume.pdf'
APPLICATION_PDF_FILE = 'Application.pdf'

# Temporary folder name and related paths
TEMP_FOLDER = 'temp'
TEMP_FOLDER_PATH = os.path.join(PROJECT_DIR_PATH, TEMP_FOLDER)
RESUME_TEMP_PATH = os.path.join(TEMP_FOLDER_PATH, RESUME_FILE)
COVER_LETTER_ODT_TEMP_PATH = os.path.join(TEMP_FOLDER_PATH, COVER_LETTER_ODT_FILE)
COVER_LETTER_PDF_TEMP_PATH = os.path.join(TEMP_FOLDER_PATH, COVER_LETTER_PDF_FILE)
COVER_LETTER_ODT_RENDERED_TEMP_PATH = os.path.join(TEMP_FOLDER_PATH, COVER_LETTER_RENDERED_ODT_FILE)
APPLICATION_TEMP_PATH = os.path.join(TEMP_FOLDER_PATH, APPLICATION_PDF_FILE)

# Utility Functions for Project paths
def getapplicationfolderpath(application_choice: str):
    """Get the application folder path based on user choice"""
    return os.path.join(PROJECT_DIR_PATH,
                        __application_folder__,
                        application_choice)

def build_resume_path(company_name: str, out_folder: str):
    """Create the resume output file path."""
    resume_file_name = str(PROJECT_USER_NAME + '_' + company_name + '_' + RESUME_FILE)
    return os.path.join(out_folder, resume_file_name)

def build_coverletter_path(company_name: str, out_folder: str):
    """Create the rendered cover letter file path."""
    coverletter_file_name = str(PROJECT_USER_NAME + '_' + company_name + '_CoverLetter.pdf')
    return os.path.join(out_folder, coverletter_file_name)

def build_merged_path(company_name: str, out_folder: str):
    """Create the merged pdf file path"""
    merged_file_name = str(PROJECT_USER_NAME + '_' + company_name + '_' + APPLICATION_PDF_FILE)
    return os.path.join(out_folder, merged_file_name)
