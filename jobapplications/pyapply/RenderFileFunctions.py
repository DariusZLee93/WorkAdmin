"""The render file functions."""
import os
import sys
import json
import traceback
import shutil

import PyApplyConstants as Consts
import OdtToPdfFunctions
import OdtFunctions

def CheckRenderCompletion():
    if not os.path.exists(Consts.COVER_LETTER_ODT_RENDERED_TEMP_PATH):
        raise ValueError("The rendered file is not there. Path: " +
                         Consts.COVER_LETTER_ODT_RENDERED_TEMP_PATH)
    else:
        print("Render generated file successfully.")

def CheckInputsAndPaths():
    if not os.path.exists(Consts.COVER_LETTER_ODT_TEMP_PATH):
        raise ValueError("The cover letter odt file is not there. Path: " +
                         Consts.COVER_LETTER_ODT_TEMP_PATH)
    elif not os.path.exists(Consts.RESUME_TEMP_PATH):
        raise ValueError("The resume pdf file is not there. Path: " +
                         Consts.RESUME_TEMP_PATH)

def CheckPdfConversionCompletion():
    if not os.path.exists(Consts.COVER_LETTER_PDF_TEMP_PATH):
        raise ValueError("The cover letter pdf file is not there. Path: " +
                         Consts.COVER_LETTER_PDF_TEMP_PATH)

def CheckMergeCompletion():
    if not os.path.exists(Consts.APPLICATION_TEMP_PATH):
        raise ValueError("The application pdf file is not there. Path: " +
                         Consts.APPLICATION_TEMP_PATH)

# Application entry point
def Render(app_data: dict):
    # Check that the environment is ready. If necessary paths don't exist then exit.
    CheckInputsAndPaths()

    # Render the template
    print("Render template: " + Consts.COVER_LETTER_ODT_TEMP_PATH + ' to path: ' +
          Consts.COVER_LETTER_ODT_RENDERED_TEMP_PATH)
    OdtFunctions.copy_odt_modify_content_xml(Consts.COVER_LETTER_ODT_TEMP_PATH,
                                             Consts.COVER_LETTER_ODT_RENDERED_TEMP_PATH,
                                             app_data)
    CheckRenderCompletion()

    # Convert
    OdtToPdfFunctions.convert_to_pdf(Consts.COVER_LETTER_ODT_RENDERED_TEMP_PATH,
                                     Consts.TEMP_FOLDER_PATH)
    CheckPdfConversionCompletion()

    # Merge PDFs
    OdtToPdfFunctions.merge_pdfs(Consts.COVER_LETTER_PDF_TEMP_PATH, Consts.RESUME_TEMP_PATH,
                                 Consts.APPLICATION_TEMP_PATH)
    CheckMergeCompletion()

def start_test_environment():
    shutil.copy()

if __name__ == "__main__":
    try:
        APP_DATA = start_test_environment()
        Render(APP_DATA)
    except Exception as e:
        print(e)
        print(traceback.format_exc())
    finally:
        input('Press enter to exit and start cleanup: ')
