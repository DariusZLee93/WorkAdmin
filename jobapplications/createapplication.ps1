$pythonFile = "CreateApplication.py"

try
{
    Write-Host "Importing Python bootstrapper."
    Import-Module .\pythonbootstrapper.psm1
    
    Write-Host "Start application entry function."
    EnterPythonEnvironment

    ExecutePythonScript $pythonFile
}
catch
{
    Write-Host ""
    $Error
    $Error.Clear()
    Write-Host "Exiting with errors."
}
finally
{
    # Clean up python environment (if necessary)
    ExitPythonEnvironment

    Remove-Module pythonbootstrapper
}