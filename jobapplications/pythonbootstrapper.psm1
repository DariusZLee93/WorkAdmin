$currentDirectory = $pwd
$pythonDirectoryName = "pyapply"
$pythonScriptDirectory = "Scripts"

$pythonActivationScript = "activate.ps1"
$pythonDeactivationScript = "deactivate"
$pythonExecutor = "python"

function EnterPythonEnvironment()
{
    $activationPath = [System.IO.Path]::Combine($currentDirectory, $pythonDirectoryName, $pythonScriptDirectory, $pythonActivationScript)
    & $activationPath
}

function ExecutePythonScript([string] $pythonFile, [string] $pythonArguments)
{
    $scriptPath = [System.IO.Path]::Combine($currentDirectory, $pythonDirectoryName, $pythonFile)
    & $pythonExecutor $scriptPath $pythonArguments 
}

function ExitPythonEnvironment()
{
    & $pythonDeactivationScript
}
