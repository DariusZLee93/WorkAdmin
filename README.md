This project is used to generate cover letters and resumes quickly.

Project written in python for fun and learning!

ODT word processor files are used for cover letters and the content.xml is extracted to insert job specific data.
The ODT is then rendered to a PDF and then the python library PYPDF2 is used to merge a Resume.pdf with the rendered CoverLetter.pdf.

Application are stored in a folder heirarchy like so: applications\{{year}{month}{date}}.

A sqlite db is used as a datastore to keep track of applications. The url of the job application is the unique key and the application should be able to be recreated based on the rest of the data in the fields.

